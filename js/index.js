const URL_API = 'http://67.205.156.112:3000/web'
const URL_BASE = `${window.location.origin}/`

verificarSession()
document.addEventListener('DOMContentLoaded', async (e) => {
    e.preventDefault()
    btnEntrar.addEventListener('click', async (e) => {
        e.preventDefault()
        const session = verificarSession()
        if (session.token !== '' && session.token !== 'undefined' && session.token !== null && session.isLogged) {
            window.location.replace(`${URL_BASE}dashboard.html`)
        } else {
            const userEmail = txtEmail.value
            const userPass = txtPassword.value
            const userData = {
                email: userEmail,
                password: userPass
            }
            const data = await iniciarSesion(userData)
            await manejarSession(data)
            verificarSession()

        }
    })
})
const iniciarSesion = async function (userData) {
    const user = JSON.stringify(userData);
    console.log(user)
    const data = await fetch(`${URL_API}/iniciar`, {
        method: 'POST',
        body: user,
        headers: { 'Content-Type': 'application/json' },
    })
        .then(async (data) => {
            return data
        })
        .catch(error => {
            console.log(error)
        })
    return data
}

const manejarSession = async (userData) => {
    if (userData.status > 300) {
        sessionStorage.clear('token')
        sessionStorage.setItem('isLogged', false)
        sessionStorage.clear('idUsuario')
        sessionStorage.clear('email')
        sessionStorage.clear('nombres')
        alert(statusCode[userData.status])
    } else {
        const { data } = await userData.json()
        sessionStorage.setItem('isLogged', true)
        sessionStorage.setItem('token', data.token)
        sessionStorage.setItem('idUsuario', data.idUsuario)
        sessionStorage.setItem('email', data.email)
        sessionStorage.setItem('nombres', data.nombres)
        alert(statusCode[userData.status])
    }
}
//function global
async function verificarSession(){
    const token = sessionStorage.getItem('token')
    const isLogged = sessionStorage.getItem('isLogged')
    if (token !== '' && token !== 'undefined' && token !== null && isLogged) {
        window.location.replace(`${URL_BASE}dashboard.html`)
    } else {
        return { token, isLogged }
    }
}
const statusCode = {
    403: 'No está autorizado o sus credenciales son incorrectas',
    422: 'Uno o alguno de los parametros requeridos no fueron proporcionados',
    500: 'Ha ocurrido un error de servidor, porfavor reportelo al administrador',
    503: 'Ha ocurrido un error de servidor, porfavor reportelo al administrador',
    200: 'Autenticación exitosa!',
    201: 'La petición fue exitosa'
}