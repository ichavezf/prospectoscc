const URL_API = 'http://67.205.156.112:3000/web'
// const URL_API = 'http://localhost:3000/web' TEST
const URL_BASE = `${window.location.origin}/`
let prospectoSeleccionado = 0;
verificarSession()

addEventListener('DOMContentLoaded', async (e) => {
    e.preventDefault()
    generarListado()

    btnSalir.addEventListener('click', (e) => {
        e.preventDefault()
        cerrarSession()
    })

    btnActualizar.addEventListener('click', async (e) => {
        e.preventDefault()
        const revisionData = {
            idProspecto: prospectoSeleccionado,
            estatus: cboEstatus.value,
            observaciones: txtObserv.value
        }
        txtObserv.value = ''
        cboEstatus.value = 0
        const response = await actualizarProspecto(revisionData)
        await generarListado()
        btnCancelar.click()
    })
    btnCancelar.addEventListener('click', (e) => {
        e.preventDefault()
        txtObserv.value = ''
        cboEstatus.value = 0
    })
})

const obtenerProspectos = async () => {
    const data = await fetch(`${URL_API}/prospectos?page=0&limit=100`, {
        method: 'GET',
        headers: { 'Authorization': `Bearer ${sessionStorage.getItem('token')}` }
    })
        .then(async (data) => {
            return await data
        })
        .catch((error) => {
            console.log(error)
        })
    return data.json()
}

const actualizarProspecto = async (revisionData) => {
    const data = await fetch(`${URL_API}/prospectos`, {
        method: 'PATCH',
        headers: {
            'Authorization': `Bearer ${sessionStorage.getItem('token')}`,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(revisionData)
    })
        .then(async (data) => {
            return await data
        })
        .catch((error) => {
            console.log(error)
        })
    return data.json()
}

const generarListado = async () => {
    const prospectosData = await obtenerProspectos();
    crearListado(prospectosData)
}

const cerrarSession = async () => {
    sessionStorage.clear('token')
    sessionStorage.setItem('isLogged', false)
    sessionStorage.clear('idUsuario')
    sessionStorage.clear('email')
    sessionStorage.clear('nombres')
    verificarSession()
}

const crearListado = async (prospectosData) => {
    if(document.getElementById('tbListado')){
        tbListado.remove()
    }
    const tableHeads = ['Id', 'Nombre completo', 'Calle', 'Numero Interior',
        'Colonia', 'Codigo Postal', 'Telefono', 'RFC', 'Estatus', 'Observaciones', 'Acciones']
    const { data } = prospectosData
    const table = document.createElement('table')
    table.setAttribute('id', 'tbListado')
    table.setAttribute('class', 'table table-striped table-bordered')
    const thead = document.createElement('thead')
    const tbody = document.createElement('tbody')
    const tr = document.createElement('tr')
    thead.appendChild(tr)
    table.appendChild(thead)

    tableHeads.forEach(element => {
        const th = document.createElement('th')
        th.setAttribute('scope', 'col')
        th.appendChild(document.createTextNode(element))
        tr.appendChild(th)
    })
    data.forEach((element, index, array) => {

        element['acciones'] = ''
        element.nombres = `${element.nombres} ${element.primerApellido} ${element.segundoApellido}`
        delete element.primerApellido
        delete element.segundoApellido
        delete element.idUsuario
        const tr = document.createElement('tr')
        for (const key in element) {
            if (element.hasOwnProperty(key)) {
                if (key == 'acciones') {
                    const button = document.createElement('button')
                    button.setAttribute('type', 'button')
                    button.setAttribute('class', 'btn btn-success btn-form')
                    button.setAttribute('data-toggle', 'modal')
                    button.setAttribute('data-target', '#modalCaptura')
                    button.setAttribute('onclick', `actualizarVariable(${element.idProspecto})`)
                    button.appendChild(document.createTextNode('Actualizar'))
                    tr.appendChild(button)
                } else {
                    const value = element[key];
                    const td = document.createElement('td')
                    td.appendChild(document.createTextNode(value))
                    tr.appendChild(td)
                }

            }
        }
        tbody.appendChild(tr)

    });
    table.appendChild(tbody)
    listadoProspectos.appendChild(table)
}

const actualizarVariable = (value) => {
    prospectoSeleccionado = value
}
//function global
async function verificarSession() {
    const token = sessionStorage.getItem('token')
    const isLogged = sessionStorage.getItem('isLogged')
    if ((token == '' || token == 'undefined' || token == null) && !isLogged) {
        window.location.replace(`${URL_BASE}index.html`)
    } else {
        return { token, isLogged }
    }
}

const statusCode = {
    403: 'No está autorizado o sus credenciales son incorrectas',
    422: 'Uno o alguno de los parametros requeridos no fueron proporcionados',
    500: 'Ha ocurrido un error de servidor, porfavor reportelo al administrador',
    503: 'Ha ocurrido un error de servidor, porfavor reportelo al administrador',
    200: 'Autenticación exitosa!',
    201: 'La petición fue exitosa'
}